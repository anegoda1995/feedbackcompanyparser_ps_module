<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

class FeedbackCompanyParser extends Module
{
    public function __construct()
    {
        $this->name = 'feedbackcompanyparser';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
            'min' => '1.6.1',
            'max' => _PS_VERSION_
        ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Feedback Company Parser');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('ORDERDIRECT_MODULE_NAME')) {
            $this->warning = $this->l('No name provided');
        }
    }

    public function install()
    {
        if (
            !parent::install() ||
            !$this->registerHook('top') ||
            !$this->registerHook('header')
        ) {
            $this->_errors[] = 'Unable to install module';

            return false;
        }
        return true;
    }

    public function uninstall()
    {
        if (
            !parent::uninstall() ||
            !$this->unregisterHook('top') ||
            !$this->unregisterHook('header')
        ) {
            return false;
        }
        return true;
    }

    public function hookHeader()
    {
        $this->context->controller->addCSS($this->_path.'views/css/reviews.css');
    }

    public function hookTop($params) {
        global $cache, $smarty;
        $fullStars = 0;
        $halfStars = 0;

        $rating = Configuration::get('FCP_RATING', false, 1, 1);
        $reviews = (int) Configuration::get('FCP_REVIEWS', false, 1, 1);
        
        $fullStars = floor($rating / 2);
        
        //half star can exist if rating double or odd
        $halfStar = !is_integer(filter_var($rating, FILTER_VALIDATE_INT)) || floor($rating) % 2 != 0  ? true : false;
        $emptyStars = 5 - ($fullStars + $halfStar);

        $smarty->assign([
			'fullStars' => (int) $fullStars,
            'halfStar' => $halfStar,
            'emptyStars' => $emptyStars,
            'rating' => $rating,
            'reviews' => $reviews,
            'url' => Configuration::get('FCP_URL', false, 1, 1)
        ]);

        return $this->display(__FILE__, $this->name . '.tpl');
    }

    public function updateRating()
    {
        $html = file_get_contents(
            Configuration::get('FCP_URL'),
            false,
            stream_context_create(array(
              "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            ))
        );
        
        // get this block
        $doc = new \DOMDocument;
        @$doc->loadHTML($html);
        $xpath = new DOMXPath($doc);
        $ratingDom = $xpath->query("//*[contains(@class, 'rating')]");
        $html = $doc->saveHtml($ratingDom[0]);

        // get rating
        $doc = new \DOMDocument;
        @$doc->loadHTML($html);
        $xpath = new DOMXPath($doc);
        $ratingDom = $xpath->query("//*[contains(@class, 'rating__numbers')]/span/text()");
        $rating = $doc->saveHtml($ratingDom[0]);
        $data['rating'] = trim($rating);
        Configuration::updateValue('FCP_RATING', $data['rating']);

        // get reviews
        $doc = new \DOMDocument;
        @$doc->loadHTML($html);
        $xpath = new DOMXPath($doc);
        $ratingDom = $xpath->query("//*[contains(@class, 'rating__review-count')]/text()");
        $reviews = $doc->saveHtml($ratingDom[0]);
        $data['reviews'] = filter_var(trim($reviews), FILTER_SANITIZE_NUMBER_INT);
        Configuration::updateValue('FCP_REVIEWS', $data['reviews']);
        
        return $data;
    }

    public function getContent()
    {
        $output = null;

        if (Tools::isSubmit('SAVE_CONFIGS')) {
            Configuration::updateValue('FCP_URL', Tools::getValue('FCP_URL'), false, 1 ,1);
            Configuration::updateValue('FCP_RATING', Tools::getValue('FCP_RATING'), false, 1 ,1);
            Configuration::updateValue('FCP_REVIEWS', Tools::getValue('FCP_REVIEWS'), false, 1 ,1);
            $output .= $this->displayConfirmation('Settings updated');
        }

        if (Tools::isSubmit('MANUALLY_UPDATING')) {
            $data = $this->updateRating();
            $output .= $this->displayConfirmation('Settings updated. Review count: ' . $data['rating'] . '. Rating: ' . $data['reviews'] . '.');
        }

        return $output.$this->displayForm();
    }

    public function displayForm()
    {
        // Get default language
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $fieldsForm[0]['form'] = [
            'legend' => [
                'title' => 'Settings',
            ],
            'input' => [
                [
                    'type' => 'text',
                    'label' => 'Url',
                    'name' => 'FCP_URL',
                    'size' => 30,
                    'required' => false
                ],
                [
                    'type' => 'text',
                    'label' => 'Rating',
                    'name' => 'FCP_RATING',
                    'size' => 30,
                    'required' => false
                ],
                [
                    'type' => 'text',
                    'label' => 'Reviews count',
                    'name' => 'FCP_REVIEWS',
                    'size' => 30,
                    'required' => false
                ],
            ],
            'submit' => [
                'title' => 'Save',
                'name' => 'SAVE_CONFIGS',
                'class' => 'btn btn-default pull-right'
            ]
        ];

        $fieldsForm[1]['form'] = [
            'legend' => [
                'title' => 'Manually updating',
            ],
            'input' => [
                [
                    'type' => 'text',
                    'enabled' => false,
                    'size' => 30,
                    'name' => 'FCP_CRON_URL'
                ]
            ]
        ];



        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language = $defaultLang;
        $helper->allow_employee_form_lang = $defaultLang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = [
            'save' => [
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules'),
            ],
            'back' => [
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ]
        ];

        // Load current value
        $helper->fields_value['FCP_URL'] = Configuration::get('FCP_URL', false, 1, 1);
        $helper->fields_value['FCP_RATING'] = Configuration::get('FCP_RATING', false, 1, 1);
        $helper->fields_value['FCP_REVIEWS'] = Configuration::get('FCP_REVIEWS', false, 1, 1);
        $helper->fields_value['FCP_CRON_URL'] = $this->cron_url = _PS_BASE_URL_ . _MODULE_DIR_ . $this->name . '/' . $this->name . '-cron.php?token=' . substr(Tools::encrypt($this->name . '/cron'), 0, 10);

        return $helper->generateForm($fieldsForm);
    }
}