<div class="rating">
    <div class="rating__stars-and-score">
        <div class="rating__stars-centerer">
            <div class="stars stars--big">
                {for $i=0 to $fullStars - 1}
                    <div class="stars__star stars__star--full"></div>
                {/for}
                {if $halfStar}
                    <div class="stars__star stars__star--half"></div>
                {/if}
                {for $i=0 to $emptyStars - 1}
                    <div class="stars__star"></div>
                {/for}
            </div>
        </div>
        <div class="rating__numbers">
            <span>
                {$rating}
            </span>
            <span class="rating__divider">/</span>
            <span>
                10
            </span>
        </div>
    </div>
    <div class="label--small rating__review-count">
        <a href="{$url}" rel="nofollow noopener" target="_blank">
        {$reviews}
        beoordelingen
        </a>
    </div>
</div>