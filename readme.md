# Feedback Company Parser

## About

Prestashop 1.6 module that allow you automatically update and show rate from feedbackcompany.com

## Requirements
Prestashop >=1.6.1

## Installing
1. Clone the **[module repository](https://bitbucket.org/anegoda1995/feedbackcompanyparser_ps_module "module repository")** in *prestashop_folder/modules/feedbackcompanyparser* folder
2. Configure it in backoffice:
	- url; (for example, https://www.feedbackcompany.com/nl-nl/reviews/rolsteiger-net/)
3. Set new row in your **cron** config for how often you want to update. Be aware, don't change the token, only paths:
	- *crontab -e*
	- *php /path/to/project/modules/feedbackcompanyparser/feedbackcompanyparser-cron.php 0e8d4289f5 >> /path/to/log 2>&1*